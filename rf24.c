#include "rf24.h"

#include "debug.h"
#include "delay.h"
#include "nRF24L01.h"

#include <string.h>

#ifndef _BV
#  define _BV(b) (1<<(b))
#endif

#if defined(__AVR__) && defined(RF24_USE_PGMSPACE)
#  include <avr/pgmspace.h>
#else
#  define PROGMEM
#  define pgm_read_byte(addr) (*(addr))
#endif

#define min(a, b) ((a) < (b) ? (a) : (b))

#ifdef DEBUG

static char dbg_buf[512];
static char* dbg_str = dbg_buf;

#define dbg_add(fmt, args...) dbg_str += snprintf(dbg_str, dbg_buf + sizeof(dbg_buf) - dbg_str, fmt, ##args);

#define dbg_hexl(name, data, size)              \
  dbg_add(#name "(");                           \
  for(int i = 0; i < size; i++){                \
    dbg_add("%02x ", data[i]);                  \
  }                                             \
  if(size) dbg_str--;                           \
  dbg_add(")");

#define dbg_bool(name) dbg_add(#name "(%x) ", val & _BV(name) ? 1 : 0);
#define dbg_enum(name, bits, cases) dbg_add(#name);                     \
  switch((val >> (name)) & ((0b1 << (bits)) - 1)){                      \
    cases;                                                              \
  default: dbg_add("(Illegal) ");                                       \
  }
#define dbg_case(val, name) case val: dbg_add( "(" #name ") "); break;
#define dbg_uint(name, bits) dbg_add(#name "(%u) ", (val >> (name)) & ~(0xff << (bits)));
#define dbg_conv(op, block) {rf24_val_t _val = (op); {rf24_val_t val = _val; block;}}

static inline const char* dbg_reg(rf24_val_t reg, rf24_val_t val, rf24_addr_t *addr){
  const char* dbg_beg = dbg_str;
  switch(reg){
#define VAL 0
#define _(reg_name, reg_data) case reg_name: dbg_add(#reg_name "{"); reg_data; dbg_str--; dbg_add("}"); break;
    _(CONFIG, dbg_bool(MASK_RX_DR) dbg_bool(MASK_TX_DS) dbg_bool(MASK_MAX_RT) dbg_bool(EN_CRC) dbg_bool(CRCO) dbg_bool(PWR_UP) dbg_bool(PRIM_RX));
    _(EN_AA, dbg_bool(ENAA_P5) dbg_bool(ENAA_P4) dbg_bool(ENAA_P3) dbg_bool(ENAA_P2) dbg_bool(ENAA_P1) dbg_bool(ENAA_P0));
    _(EN_RXADDR, dbg_bool(ERX_P5) dbg_bool(ERX_P4) dbg_bool(ERX_P3) dbg_bool(ERX_P2) dbg_bool(ERX_P1) dbg_bool(ERX_P0));
    _(SETUP_AW, dbg_enum(AW, 2, dbg_case(1, 3) dbg_case(2, 4) dbg_case(3, 5)));
    _(SETUP_RETR, dbg_enum(ARD, 4, dbg_case(0, 250uS) dbg_case(1, 500uS) dbg_case(2, 750uS) dbg_case(3, 1000uS) dbg_case(4, 1250uS) dbg_case(5, 1500uS) dbg_case(6, 1750uS) dbg_case(7, 2000uS) dbg_case(8, 2250uS) dbg_case(9, 2500uS) dbg_case(10, 2750uS) dbg_case(11, 3000uS) dbg_case(12, 3250uS) dbg_case(13, 3500uS) dbg_case(14, 3750uS) dbg_case(15, 4000uS)) dbg_uint(ARC, 4));
    _(RF_CH, dbg_uint(VAL, 7));
    _(RF_SETUP, dbg_bool(CONT_WAVE) dbg_bool(PLL_LOCK) dbg_conv((val & RF_DR_HIGH) | ((val & RF_DR_LOW) >> 1), dbg_enum(RF_DR, 2, dbg_case(0, 1Mbps) dbg_case(1, 2Mbps) dbg_case(2, 250kbps))) dbg_enum(RF_PWR, 2, dbg_case(0, -18dBm) dbg_case(1, -12dBm) dbg_case(2, -6dBm) dbg_case(3, 0Bm)));
    _(STATUS, dbg_bool(RX_DR) dbg_bool(TX_DS) dbg_bool(MAX_RT) dbg_uint(RX_P_NO, 3) dbg_bool(TX_FULL));
    _(OBSERVE_TX, dbg_uint(PLOS_CNT, 4) dbg_uint(ARC_CNT, 4));
    _(RPD, dbg_bool(RPD));
    _(RX_ADDR_P0, dbg_hexl(VAL, addr->_5, 5));
    _(RX_ADDR_P1, dbg_hexl(VAL, addr->_5, 5));
    _(RX_ADDR_P2, dbg_hexl(VAL, addr->_5, 1));
    _(RX_ADDR_P3, dbg_hexl(VAL, addr->_5, 1));
    _(RX_ADDR_P4, dbg_hexl(VAL, addr->_5, 1));
    _(RX_ADDR_P5, dbg_hexl(VAL, addr->_5, 1));
    _(TX_ADDR, dbg_hexl(ADDR, addr->_5, 5));
    _(RX_PW_P0, dbg_uint(VAL, 6));
    _(RX_PW_P1, dbg_uint(VAL, 6));
    _(RX_PW_P2, dbg_uint(VAL, 6));
    _(RX_PW_P3, dbg_uint(VAL, 6));
    _(RX_PW_P4, dbg_uint(VAL, 6));
    _(RX_PW_P5, dbg_uint(VAL, 6));
    _(FIFO_STATUS, dbg_bool(TX_REUSE) dbg_bool(TX_FULL) dbg_bool(TX_EMPTY) dbg_bool(RX_FULL) dbg_bool(RX_EMPTY));
    _(DYNPD, dbg_bool(DPL_P5) dbg_bool(DPL_P4) dbg_bool(DPL_P3) dbg_bool(DPL_P2) dbg_bool(DPL_P1) dbg_bool(DPL_P0));
    _(FEATURE, dbg_bool(EN_DPL) dbg_bool(EN_ACK_PAY) dbg_bool(EN_DYN_ACK));
#undef _
#undef VAL
  }
  dbg_str++;
  return dbg_beg;
}

#define DBG_REG(reg, val) dbg_reg(reg, val, NULL)
#define DBG_REG_ADDR(reg, val) dbg_reg(reg, 0, val)
#define DBG_HEX(data, size) ({ const char *dbg_beg = dbg_str; dbg_hexl(VAL, (data), (size)); dbg_str++; dbg_beg; })

#define DBG_LOG(fmt, arg...) { dbg_str = dbg_buf; LOG(fmt, ##arg); }

#else

#define DBG_LOG(fmt, arg...)

#endif

static rf24_val_t _rf24_spi_io(rf24_dev_t *dev, rf24_val_t val){
  rf24_val_t ret;
  
  spi_transfer(&dev->spi, &val, &ret, 1);
  
  return ret;
}

static rf24_val_t _rf24_read_register(rf24_dev_t *dev, rf24_val_t reg, rf24_val_t *val){
  rf24_val_t tx[] = {
    R_REGISTER | (REGISTER_MASK & reg),
    0xff
  }, rx[sizeof(tx)];
  
  spi_transfer(&dev->spi, tx, rx, sizeof(tx));
  *val = rx[1];
  
  DBG_LOG("  read_register\n    %s\n  ! %s", DBG_REG(reg, *val), reg != STATUS ? DBG_REG(STATUS, rx[0]) : "");
  
  return rx[0];
}

static rf24_val_t _rf24_read_register_val(rf24_dev_t *dev, rf24_val_t reg){
  rf24_val_t val;
  
  _rf24_read_register(dev, reg, &val);
  
  return val;
}

static rf24_val_t _rf24_write_register(rf24_dev_t *dev, rf24_val_t reg, rf24_val_t val){
  rf24_val_t tx[] = {
    W_REGISTER | (REGISTER_MASK & reg),
    val
  }, rx[sizeof(tx)];
  
  spi_transfer(&dev->spi, tx, rx, sizeof(tx));
  
  DBG_LOG("  write_register\n    %s\n  ! %s", DBG_REG(reg, val), reg != STATUS ? DBG_REG(STATUS, rx[0]) : "");
  
  return rx[0];
}

static rf24_val_t _rf24_write_register_addr(rf24_dev_t *dev, rf24_val_t reg, rf24_addr_t *val){
  rf24_val_t tx[1 + sizeof(rf24_addr_t)] = {
    W_REGISTER | (REGISTER_MASK & reg)
  }, rx[sizeof(tx)];
  
  memcpy(&tx[1], val, sizeof(rf24_addr_t));
  
  spi_transfer(&dev->spi, tx, rx, sizeof(tx));
  
  DBG_LOG("  write_register_addr\n    %s", DBG_REG_ADDR(reg, val));
  
  return rx[0];
}

static rf24_val_t _rf24_read_payload(rf24_dev_t *dev, rf24_raw_t *buf, rf24_len_t *len){
  rf24_len_t data_len = min(*len, dev->dynamic_payloads ? rf24_get_dynamic_payload_size(dev) : dev->payload_size);
  rf24_len_t full_len = 1 + data_len + (dev->dynamic_payloads ? 0 : (dev->payload_size - data_len));
  
  rf24_val_t tx[full_len], rx[full_len];
  
  tx[0] = R_RX_PAYLOAD;
  memset(&tx[1], 0xff, full_len - 1);
  
  spi_transfer(&dev->spi, tx, rx, full_len);
  
  memcpy(buf, &rx[1], data_len);

  DBG_LOG("  read_payload\n    %s\t#%d\n  ! %s", DBG_HEX(rx + 1, data_len), data_len, DBG_REG(STATUS, rx[0]));
  
  *len = data_len;
  
  return rx[0];
}

static rf24_val_t _rf24_write_payload(rf24_dev_t *dev, const rf24_raw_t *buf, rf24_len_t len){
  rf24_len_t data_len = min(len, dev->payload_size);
  rf24_len_t free_len = dev->dynamic_payloads ? 0 : (dev->payload_size - data_len);
  rf24_len_t full_len = 1 + data_len + free_len;
  
  rf24_val_t tx[full_len], rx[full_len];
  
  tx[0] = W_TX_PAYLOAD;
  memcpy(&tx[1], buf, data_len);
  memset(&tx[1 + data_len], 0x00, free_len);
  
  spi_transfer(&dev->spi, tx, rx, full_len);
  
  DBG_LOG("  write_payload\n    %s\t#%d\n  ! %s", DBG_HEX(tx + 1, data_len), data_len, DBG_REG(STATUS, rx[0]));
  
  return rx[0];
}

static void _rf24_flush_rx(rf24_dev_t *dev){
  _rf24_spi_io(dev, FLUSH_RX);
  LOG("  flush_rx");
}

static void _rf24_flush_tx(rf24_dev_t *dev){
  _rf24_spi_io(dev, FLUSH_TX);
  LOG("  flush_tx");
}

#define _rf24_tx_data_sent(status) ((status) & _BV(TX_DS))
#define _rf24_tx_max_retry(status) ((status) & _BV(MAX_RT))
#define _rf24_tx_data_sent_or_max_retry(status) ((status) & (_BV(TX_DS) | _BV(MAX_RT)))
#define _rf24_rx_data_ready(status) ((status) & _BV(RX_DR))

static rf24_val_t _rf24_get_status(rf24_dev_t *dev){
  rf24_val_t status;
  
  status = _rf24_spi_io(dev, NOP);

  DBG_LOG("  get_status\n  ! %s", DBG_REG(STATUS, status));
  
  return status;
}

static void _rf24_reset_status(rf24_dev_t *dev){
  _rf24_write_register(dev, STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));
}

static void _rf24_reset_config(rf24_dev_t *dev){
  _rf24_write_register(dev, CONFIG, 0x0f);
}

void rf24_open(rf24_dev_t *dev, spi_hwid_t spi, gpio_pin_t ce){
  spi_open(&dev->spi, spi, SPI_MODE_DEFAULT, 8, 8000000);
  gpio_open(&dev->ce, ce);
  
  dev->pipe0_address._ = 0;
  
  gpio_set_dir(&dev->ce, 1);
  gpio_set_val(&dev->ce, 0);

  // Adjustments as per gcopeland fork
  _rf24_reset_config(dev);
  
  // Must allow the radio time to settle else configuration bits will not necessarily stick.
  // This is actually only required following power up but some settling time also appears to
  // be required after resets too. For full coverage, we'll always assume the worst.
  // Enabling 16b CRC is by far the most obvious case if the wrong timing is used - or skipped.
  // Technically we require 4.5ms + 14us as a worst case. We'll just call it 5ms for good measure.
  // WARNING: Delay is based on P-variant whereby non-P *may* require different timing.
  _delay_ms(5);
  
#if defined(RF24_DEFAULT_RETRY_DELAY) && defined(RF24_DEFAULT_RETRY_COUNT)
  // Set 1500uS (minimum for 32B payload in ESB@250KBPS) timeouts, to make testing a little easier
  // WARNING: If this is ever lowered, either 250KBS mode with AA is broken or maximum packet
  // sizes must never be used. See documentation for a more complete explanation.
  rf24_set_retry(dev, RF24_DEFAULT_RETRY_DELAY, RF24_DEFAULT_RETRY_COUNT);
#endif
  
#if defined(RF24_DEFAULT_POWER_AMP)
  // Restore our default PA level
  rf24_set_power_amp(dev, RF24_DEFAULT_POWER_AMP);
#endif
  
  // Determine if this is a p or non-p RF24 module and then
  // reset our data rate back to default value. This works
  // because a non-P variant won't allow the data rate to
  // be set to 250Kbps.
  if(rf24_set_data_rate(dev, RF24_DATA_RATE_250KBS)){
    dev->hardware = RF24_HARDWARE_RF24L01P;
  }else{
    dev->hardware = RF24_HARDWARE_RF24L01;
  }
  
#if defined(RF24_DEFAULT_DATA_RATE)
  // Then set the data rate to the slowest (and most reliable) speed supported by all
  // hardware.
  rf24_set_data_rate(dev, RF24_DEFAULT_DATA_RATE);
#endif
  
#if defined(RF24_DEFAULT_CRC)
  // Initialize CRC and request 2-byte (16bit) CRC
  rf24_set_crc(dev, RF24_DEFAULT_CRC);
#endif

#if defined(RF24_DEFAULT_PAYLOAD_SIZE)
  // Initialize default payload size
  rf24_set_payload_size(dev, RF24_DEFAULT_PAYLOAD_SIZE);
#endif
  
#if defined(RF24_DEFAULT_DYNAMIC_PAYLOADS)
  // Disable dynamic payloads, to match dynamic_payloads setting
  rf24_set_dynamic_payloads(dev, RF24_DEFAULT_DYNAMIC_PAYLOADS);
#endif
  
  // Reset current status
  // Notice reset and flush is the last thing we do
  _rf24_reset_status(dev);
  
#if defined(RF24_DEFAULT_CHANNEL)
  // Set up default configuration.  Callers can always change it later.
  // This channel should be universally safe and not bleed over into adjacent
  // spectrum.
  rf24_set_channel(dev, RF24_DEFAULT_CHANNEL);
#endif
  
  // Flush buffers
  _rf24_flush_rx(dev);
  _rf24_flush_tx(dev);
  
  dev->ack_payload_size = 0;
}

void rf24_close(rf24_dev_t *dev){
  // Flush buffers
  _rf24_flush_rx(dev);
  _rf24_flush_tx(dev);
  
  gpio_set_dir(&dev->ce, 0);
}

void rf24_set_listen(rf24_dev_t *dev, rf24_val_t state){
  LOG("listen=%u", state);
  
  if(state){
    _rf24_write_register(dev, CONFIG, _rf24_read_register_val(dev, CONFIG) | _BV(PWR_UP) | _BV(PRIM_RX));
    _rf24_write_register(dev, STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));
    
    // Restore the pipe0 address, if exists
    if(dev->pipe0_address._){
      _rf24_write_register_addr(dev, RX_ADDR_P0, &dev->pipe0_address);
    }
    
    // Adjustments as per gcopeland fork
    // Flush buffers
    _rf24_flush_rx(dev);
    _rf24_flush_tx(dev);
    
    gpio_set_val(&dev->ce, 1);
    
    // wait for the radio to come up (130us actually only needed)
    _delay_us(130);
  }else{
    gpio_set_val(&dev->ce, 0);
    
    _rf24_flush_tx(dev);
    _rf24_flush_rx(dev);
    
    _rf24_write_register(dev, CONFIG, (_rf24_read_register_val(dev, CONFIG) | _BV(PWR_UP)) & ~_BV(PRIM_RX));
  }
}

void rf24_set_power(rf24_dev_t *dev, rf24_val_t state){
  LOG("power=%u", state);
  
  rf24_val_t config = _rf24_read_register_val(dev, CONFIG);
  if(state){
    config |= _BV(PWR_UP);
  }else{
    config &= ~_BV(PWR_UP);
  }
  _rf24_write_register(dev, CONFIG, config);
  if(state){
    // Adjustments as per gcopeland fork
    _delay_us(150);
  }
}

rf24_write_poll_t rf24_write_poll(rf24_dev_t *dev, rf24_write_stat_t *stat){
  LOG("write_poll");
  
  rf24_val_t observe_tx;
  rf24_val_t status = _rf24_read_register(dev, OBSERVE_TX, &observe_tx);

  //if(dev->ack_payload_size){
  // Reset last ask payload
  dev->ack_payload_size = 0;
  //}

  if(stat){
    stat->loss = (observe_tx >> PLOS_CNT) & 0xf;
    stat->retr = (observe_tx >> ARC_CNT) & 0xf;
  }
  
  if(_rf24_rx_data_ready(status)){
    dev->ack_payload_size = rf24_get_dynamic_payload_size(dev);
    LOG("ack_packet(%u)", dev->ack_payload_size);
  }
  
  if(_rf24_tx_max_retry(status)){
    return RF24_WRITE_MAX_RETRY;
  }
  if(_rf24_tx_data_sent(status)){
    return RF24_WRITE_COMPLETE;
  }
  return RF24_WRITE_ACTIVE;
}

rf24_write_poll_t rf24_write_wait(rf24_dev_t *dev, rf24_write_stat_t *stat){
  rf24_write_poll_t status;
  
  for(; (status = rf24_write_poll(dev, stat)) == RF24_WRITE_ACTIVE; ){
#if defined(RF24_WRITE_POLL_INTERVAL)
    _delay_us(RF24_WRITE_POLL_INTERVAL);
#endif
  }
  
  _rf24_reset_status(dev);
  
  // Disable powerDown and flush_tx as per gcopeland fork
  //rf24_set_power(dev, 0);
  //_rf24_flush_tx(dev);
  
  return status;
}

void rf24_write(rf24_dev_t *dev, const rf24_raw_t *buf, rf24_len_t len){
  LOG("write");
  
  // Send the payload
  _rf24_write_payload(dev, buf, len);
  
  // Allons!
  gpio_set_val(&dev->ce, 1);
  _delay_us(10);
  gpio_set_val(&dev->ce, 0);
}

rf24_len_t rf24_get_dynamic_payload_size(rf24_dev_t *dev){
  rf24_val_t tx[] = {
    R_RX_PL_WID,
    0xff
  }, rx[sizeof(tx)];
  
  spi_transfer(&dev->spi, tx, rx, sizeof(tx));
  
  return rx[1];
}

rf24_val_t rf24_read_wait_(rf24_dev_t *dev, rf24_val_t *pipe, rf24_val_t time){
  if(time){
    for(; !rf24_read_poll(dev, pipe) && time; time --){
#if defined(RF24_READ_POLL_INTERVAL)
      _delay_us(RF24_READ_POLL_INTERVAL);
#endif
    }
    return time;
  }
  for(; !rf24_read_poll(dev, pipe); ){
#if defined(RF24_READ_POLL_INTERVAL)
    _delay_us(RF24_READ_POLL_INTERVAL);
#endif
  }
  return 1;
}

rf24_val_t rf24_read_poll(rf24_dev_t *dev, rf24_val_t *pipe){
  LOG("read_poll");
  
  rf24_val_t status = _rf24_get_status(dev);

  // Too noisy, enable if you really want lots o data!!
  //IF_SERIAL_DEBUG(print_status(status));
  
  if(status & _BV(RX_DR)){
    // If the caller wants the pipe number, include that
    if(pipe){
      *pipe = (status >> RX_P_NO) & 0b111;
    }
    // Clear the status bit
    
    // ??? Should this REALLY be cleared now?  Or wait until we
    // actually READ the payload?
    
    _rf24_write_register(dev, STATUS, _BV(RX_DR));
    
    // Handle ack payload receipt
    if(status & _BV(TX_DS)){
      _rf24_write_register(dev, STATUS, _BV(TX_DS));
    }
    return 1;
  }
  
  return 0;
}

rf24_val_t rf24_read(rf24_dev_t *dev, rf24_raw_t *buf, rf24_len_t *len){
  LOG("read");
  
  // Fetch the payload
  _rf24_read_payload(dev, buf, len);
  
  // was this the last of the data available?
  return _rf24_read_register_val(dev, FIFO_STATUS) & _BV(RX_EMPTY);
}

rf24_queue_stat_t rf24_get_queue_stat(rf24_dev_t *dev){
  rf24_val_t status = _rf24_read_register_val(dev, FIFO_STATUS);
  rf24_queue_stat_t stats = {
    .read_empty = status & _BV(RX_EMPTY),
    .read_full = status & _BV(RX_FULL),
    .write_empty = status & _BV(TX_EMPTY),
    .write_full = status & _BV(TX_FULL),
    .write_reuse = status & _BV(TX_REUSE),
  };
  return stats;
}

void rf24_open_writing_pipe(rf24_dev_t *dev, rf24_addr_t *addr){
  LOG("open_writing_pipe");
  
  // Note that AVR 8-bit uC's store this LSB first, and the NRF24L01(+)
  // expects it LSB first too, so we're good.
  
  _rf24_write_register_addr(dev, TX_ADDR, addr);
  _rf24_write_register_addr(dev, RX_ADDR_P0, addr);
  
  _rf24_write_register(dev, RX_PW_P0, min(dev->payload_size, RF24_PAYLOAD_SIZE_MAX));
}

static const rf24_val_t RX_ADDR_P[] PROGMEM = {
  RX_ADDR_P0, RX_ADDR_P1, RX_ADDR_P2, RX_ADDR_P3, RX_ADDR_P4, RX_ADDR_P5
};

static const rf24_val_t RX_PW_P[] PROGMEM = {
  RX_PW_P0, RX_PW_P1, RX_PW_P2, RX_PW_P3, RX_PW_P4, RX_PW_P5
};

static const rf24_val_t ERX_P[] PROGMEM = {
  ERX_P0, ERX_P1, ERX_P2, ERX_P3, ERX_P4, ERX_P5
};

void rf24_open_reading_pipe(rf24_dev_t *dev, rf24_val_t child, rf24_addr_t *addr){
  LOG("open_reading_pipe");
  
  // If this is pipe 0, cache the address.  This is needed because
  // open_writing_pipe() will overwrite the pipe 0 address, so
  // set_listen() will have to restore it.
  if(child == 0){
    dev->pipe0_address = *addr;
  }
  
  if(child <= 6){
    // For pipes 2-5, only write the LSB
    if(child < 2){
      _rf24_write_register_addr(dev, pgm_read_byte(&RX_ADDR_P[child]), addr);
    }else{
      _rf24_write_register(dev, pgm_read_byte(&RX_ADDR_P[child]), addr->_1[0]);
    }
    
    _rf24_write_register(dev, pgm_read_byte(&RX_PW_P[child]), dev->payload_size);
    
    // Note it would be more efficient to set all of the bits for all open
    // pipes at once.  However, I thought it would make the calling code
    // more simple to do it this way.
    _rf24_write_register(dev, EN_RXADDR, _rf24_read_register_val(dev, EN_RXADDR) | _BV(pgm_read_byte(&ERX_P[child])));
  }
}

static void _rf24_toggle_features(rf24_dev_t *dev){
  rf24_val_t tx[] = {
    ACTIVATE,
    0x73
  };
  
  spi_transfer(&dev->spi, tx, NULL, sizeof(tx));
}

void rf24_set_dynamic_payloads(rf24_dev_t *dev, rf24_val_t state){
  LOG("dynamic_payloads=%u", state);
  
  if(state){
    // Enable dynamic payload throughout the system
    _rf24_write_register(dev, FEATURE, _rf24_read_register_val(dev, FEATURE) | _BV(EN_DPL));
    
    // If it didn't work, the features are not enabled
    if(!_rf24_read_register_val(dev, FEATURE)){
      // So enable them and try again
      _rf24_toggle_features(dev);
      _rf24_write_register(dev, FEATURE, _rf24_read_register_val(dev, FEATURE) | _BV(EN_DPL));
    }
    
    // Enable dynamic payload on all pipes
    //
    // Not sure the use case of only having dynamic payload on certain
    // pipes, so the library does not support it.
    _rf24_write_register(dev, DYNPD, _rf24_read_register_val(dev, DYNPD) | _BV(DPL_P5) | _BV(DPL_P4) | _BV(DPL_P3) | _BV(DPL_P2) | _BV(DPL_P1) | _BV(DPL_P0));
    
    dev->dynamic_payloads = 1;
  }else{
    _rf24_write_register(dev, DYNPD, 0);
    
    dev->dynamic_payloads = 0;
  }
}

void rf24_set_ack_payload(rf24_dev_t *dev, rf24_val_t state){
  LOG("ack_payload=%u", state);
  
  if(state){
    _rf24_write_register(dev, FEATURE, _rf24_read_register_val(dev, FEATURE) | _BV(EN_ACK_PAY) | _BV(EN_DPL));
    
    // If it didn't work, the features are not enabled
    if(!_rf24_read_register_val(dev, FEATURE)){
      // So enable them and try again
      _rf24_toggle_features(dev);
      _rf24_write_register(dev, FEATURE, _rf24_read_register_val(dev, FEATURE) | _BV(EN_ACK_PAY) | _BV(EN_DPL));
    }
    
    //
    // Enable dynamic payload on pipes 0 & 1
    //
    
    _rf24_write_register(dev, DYNPD, _rf24_read_register_val(dev, DYNPD) | _BV(DPL_P1) | _BV(DPL_P0));
  }else{
    _rf24_write_register(dev, FEATURE, _rf24_read_register_val(dev, FEATURE) & ~_BV(EN_ACK_PAY));
    
  }
}

void rf24_write_ack_payload(rf24_dev_t *dev, rf24_val_t pipe, const rf24_raw_t *buf, rf24_len_t len){
  LOG("write_ack_payload");
  
  rf24_len_t data_len = min(len, RF24_PAYLOAD_SIZE_MAX);
  
  rf24_val_t tx[1 + data_len];
  
  tx[0] = W_ACK_PAYLOAD | (pipe & 0b111);
  memcpy(&tx[1], buf, data_len);
  
  spi_transfer(&dev->spi, tx, NULL, sizeof(tx));
}

void rf24_set_auto_ack(rf24_dev_t *dev, rf24_val_t state){
  LOG("auto_ack=%u", state);
  
  if(state){
    _rf24_write_register(dev, EN_AA, 0b111111);
  }else{
    _rf24_write_register(dev, EN_AA, 0);
  }
}

void rf24_set_auto_ack_pipe(rf24_dev_t *dev, rf24_val_t pipe, rf24_val_t state){
  LOG("pipe %u auto_ack=%u", pipe, state);
  
  if(pipe <= 6){
    rf24_val_t en_aa = _rf24_read_register_val(dev, EN_AA);
    if(state){
      en_aa |= _BV(pipe);
    }else{
      en_aa &= ~_BV(pipe);
    }
    _rf24_write_register(dev, EN_AA, en_aa);
  }
}

rf24_val_t rf24_test_carrier(rf24_dev_t *dev){
  LOG("test_carrier");
  
  return _rf24_read_register_val(dev, CD) & 1;
}

rf24_val_t rf24_test_rpd(rf24_dev_t *dev){
  LOG("test_rpd");
  
  return _rf24_read_register_val(dev, RPD) & 1;
}

void rf24_set_channel(rf24_dev_t *dev, rf24_channel_t chan){
  LOG("channel=%u", chan);
  
  _rf24_write_register(dev, RF_CH, chan);
}

rf24_channel_t rf24_get_channel(rf24_dev_t *dev){
  return _rf24_read_register_val(dev, RF_CH);
}

void rf24_set_power_amp(rf24_dev_t *dev, rf24_power_amp_t amp){
  LOG("power_amp=%u", amp);
  
  uint8_t setup = _rf24_read_register_val(dev, RF_SETUP) & ~(_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH));
  
  if(amp & RF24_POWER_AMP_LOW){
    setup |= _BV(RF_PWR_LOW);
  }
  if(amp & RF24_POWER_AMP_HIGH){
    setup |= _BV(RF_PWR_HIGH);
  }
  
  _rf24_write_register(dev, RF_SETUP, setup);
}

rf24_power_amp_t rf24_get_power_amp(rf24_dev_t *dev){
  rf24_val_t setup = _rf24_read_register_val(dev, RF_SETUP);
  
  return (setup & _BV(RF_PWR_LOW) ? RF24_POWER_AMP_LOW : RF24_POWER_AMP_MIN)
    | (setup & _BV(RF_PWR_HIGH) ? RF24_POWER_AMP_HIGH : RF24_POWER_AMP_MIN);
}

rf24_val_t rf24_set_data_rate(rf24_dev_t *dev, rf24_data_rate_t rate){
  LOG("data_rate=%u", rate);
  
  rf24_val_t setup = _rf24_read_register_val(dev, RF_SETUP) & ~(_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));
  
  if(rate == RF24_DATA_RATE_250KBS){
    setup |= _BV(RF_DR_LOW);
  }else if(rate == RF24_DATA_RATE_2MBS){
    setup |= _BV(RF_DR_HIGH);
  }
  
  _rf24_write_register(dev, RF_SETUP, setup);
  
  return _rf24_read_register_val(dev, RF_SETUP) == setup;
}

rf24_data_rate_t rf24_get_data_rate(rf24_dev_t *dev){
  rf24_val_t setup = _rf24_read_register_val(dev, RF_SETUP) & (_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));
  
  return setup == _BV(RF_DR_LOW) ? RF24_DATA_RATE_250KBS : setup == _BV(RF_DR_HIGH) ? RF24_DATA_RATE_2MBS : RF24_DATA_RATE_1MBS;
}

void rf24_set_crc(rf24_dev_t *dev, rf24_crc_t crc){
  LOG("crc=%u", crc);
  
  rf24_val_t config = _rf24_read_register_val(dev, CONFIG) & ~(_BV(CRCO) | _BV(EN_CRC));
  
  if(crc != RF24_CRC_NO){
    config |= _BV(EN_CRC);
    
    if(crc == RF24_CRC_16){
      config |= _BV(CRCO);
    }
  }
  
  _rf24_write_register(dev, CONFIG, config);
}

rf24_crc_t rf24_get_crc(rf24_dev_t *dev){
  rf24_val_t config = _rf24_read_register_val(dev, CONFIG);
  
  return config & _BV(EN_CRC) ? (config & _BV(CRCO) ? RF24_CRC_16 : RF24_CRC_8) : RF24_CRC_NO;
}

void rf24_set_retry_(rf24_dev_t *dev, rf24_retry_t retry){
  LOG("retry=%u", retry);
  
  _rf24_write_register(dev, SETUP_RETR, retry);
}
