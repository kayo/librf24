# Overview

This is a pure **C** library for interaction with **Nordic®** **nRF24L01** radio hardware.
Initially it was been developed as multi-instance fork of [kehribar/nrf24L01_plus](https://github.com/kehribar/nrf24L01_plus).
But later we adopted many parts from RF24 C++ library ([maniacbug](https://github.com/maniacbug/RF24), [gnulnulf](https://github.com/gnulnulf/RF24), [stanleyseow](https://github.com/stanleyseow/RF24)).

# Why?

The primary goal of this project is a crossplatform portability. Portability in broadest sense,
so we can be able to use it both on bare hardware and on OS environment in same way.

# Current status

Currently library supports:

-   Mid-end embedded Linux with SPI/GPIO hardware interfaces/kernel drivers

-   Low-end AVR® AT(mega/tiny) 8-bit micro-controllers

# Wiring example

-   ATmega32 (Hardware SPI):
    
    -   CE  - PB3/OC0
    
    -   CSN - PB4/^SS
    
    -   SCK - PB7/SCK
    
    -   MO  - PB5/MOSI
    
    -   MI  - PB6/MISO
    
    -   IRQ - PB2/INT2 (currently don't used)

-   Raspberry Pi (Hardware SPI /dev/spidev0.1):
    
    -   CE  - (GPIO 27)
    
    -   CSN - CE1 (GPIO 7)
    
    -   SCK - SCLK (GPIO 11)
    
    -   MO  - MOSI (GPIO 10)
    
    -   MI  - MISO (GPIO 9)
    
    -   IRQ - none (currently don't used)

# Porting guidelines

If you want to port this library on unsupported hardware, all you need is write `spi-{your_platform}.c` and `gpio-{your_platform}.c`.
See spi-avrat.c and gpio-avrat.c for example.
