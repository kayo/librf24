#ifndef __SPI_H__
#define	__SPI_H__

#include <stdint.h>

typedef uint8_t spi_hwid_t;
typedef /*enum _spi_ret*/ uint8_t spi_ret_t;
typedef struct _spi_dev spi_dev_t;

typedef /*enum _spi_mode*/ uint8_t spi_mode_t;
typedef uint8_t spi_bits_t;
typedef uint32_t spi_rate_t;

enum _spi_mode{
  SPI_MODE_DEFAULT = 0,
  
  SPI_CLK_MODE = 0b11,
  SPI_CLK_PHASE = 0b01,
  SPI_CLK_POLARITY = 0b10,
  
  SPI_CSN_MODE = 0b11 << 2,
  SPI_CSN_LO = 0b00 << 2,
  SPI_CSN_HI = 0b01 << 2,
  SPI_CSN_NO = 0b10 << 2,
  
  SPI_MSB_FRST = 0b0 << 4,
  SPI_LSB_FRST = 0b1 << 4
};

//void spi_init(spi_dev_t *dev);
spi_ret_t spi_open(spi_dev_t *dev, spi_hwid_t hwid, spi_mode_t mode, spi_bits_t bits, spi_rate_t rate);

spi_ret_t spi_close(spi_dev_t *dev);

struct _spi_dev{
  spi_mode_t mode;
  spi_bits_t bits;
  spi_rate_t rate;
  
#ifdef __unix__
  int _fd;
#endif

#ifdef __AVR__
  
#endif
};

typedef uint8_t spi_len_t;
typedef uint8_t spi_val_t;

#define SPI_NO_BUFFER NULL

spi_ret_t spi_transfer(spi_dev_t *dev, const spi_val_t *tx, spi_val_t *rx, spi_len_t len);

enum _spi_ret{
  SPI_NO_ERROR = 0,
  SPI_CANT_OPEN_DEVICE,
  SPI_CANT_SET_MODE,
  SPI_CANT_SET_BITS,
  SPI_CANT_SET_RATE,
  SPI_NOT_OPENED,
  SPI_CANT_TRANSFER_MSG
};

#ifdef SPI_ERROR_STRING
const char* spi_error(spi_ret_t ret);
#endif

#endif//__SPI_H__
