default: all

# parameters parser (convert A:B:C form to _(A,B,C) for define)
comachar=,
joinwords=$(subst $(eval) ,$(2),$(1))
parsespec=$(call joinwords,$(patsubst %,_(%),$(subst :,$(comachar),$(1))))

# calc
calc=$(shell echo '$(1)' | bc)

# converts 16MHZ form to 16000000 for define
freq-hertz=$(call calc,$(subst HZ,,$(subst KHZ,*1000,$(subst MHZ,*1000*1000,$(1))))/1)

# converts 32K form to 32768 for define
size-bytes=$(call calc,$(subst K,*1024,$(subst M,*1024*1024,$(1)))/1)

# converts dec to hex
dec2hex=$(call calc,ibase=10;obase=16;$(1))

# converts hex to dec
hex2dec=$(call calc,ibase=16;obase=10;$(1))

# version parser
version-digits=$(subst ., ,$(1))
version-digit=$(word $(2),$(call version-digits,$(1)))

-include config
-include $(device).config
-include $(platform).platform
-include *.mk

src.c=$(wildcard *.c)
src.S=$(wildcard *.S)

obj+=$(src.c:.c=.o) $(src.S:.S=.o)

ifneq ($(cc.warn),)
  CFLAGS+=$(addprefix -W,$(cc.warn))
endif

ifneq ($(cc.stdc),)
  CFLAGS+=-std=$(cc.stdc)
endif

CFLAGS+=$(addprefix -I,$(cc.dir))
CFLAGS+=$(addprefix -D,$(def))

ifneq ($(debug),)
  CFLAGS+=-g
  def+=DEBUG=1
else
  ifneq ($(cc.opt),)
    CFLAGS+=-O$(cc.opt)
  endif
endif

ifneq ($(ld.map),)
  LDFLAGS+=-Wl,-Map,$*.map
endif

LDFLAGS+=$(addprefix -L,$(ld.dir))
LDFLAGS+=$(addprefix -l,$(ld.lib))

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $^

%.s: %.c
	$(CC) $(CFLAGS) -S -o $@ $^

%.o: %.S
	$(CC) $(CFLAGS) -x assembler-with-cpp -c -o $@ $^

%.s: %.S
	$(CC) $(CFLAGS) -x assembler-with-cpp -E -o $@ $^

%.e: %.c
	$(CC) $(CFLAGS) -E -o $@ $^

hex: $(project).hex
%.hex: %.elf
	$(OBJCOPY) -j .text -j .data -O ihex $< $@

srec: $(project).srec
%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

bin: $(project).bin
%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@

eep: $(project).eep
%.eep: %.elf
	$(OBJCOPY) -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 --no-change-warnings -O binary $< $@ || exit 0

lst: $(project).lst
%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

elf: $(project).elf
%.elf: $(obj)
	$(CC) $(LDFLAGS) $(LIBS) -o $@ $^

runable: $(call ld.runable,$(project))
$(call ld.runable,%): $(obj)
	$(CC) $(LDFLAGS) $(LIBS) -o $@ $^

ld.shared.full=$(call ld.shared,%,$(version))
ld.shared.major=$(call ld.shared,%,$(call version-digit,$(version),1))
ld.shared.short=$(call ld.shared,%)

shared: $(call ld.shared,$(project))
ifneq ($(ld.shared.short),$(ld.shared.major))
$(ld.shared.short): $(ld.shared.major)
	rm -f $@
	ln -s $< $@
.PRECIOUS: $(ld.shared.major)
endif
ifneq ($(ld.shared.major),$(ld.shared.full))
$(ld.shared.major): $(ld.shared.full)
	rm -f $@
	ln -s $< $@
.PRECIOUS: $(ld.shared.full)
endif
$(ld.shared.full): $(obj)
	$(CC) -shared $(LDFLAGS) $(LIBS) -o $@ $^

static: $(call ld.static,$(project))
$(call ld.static,%): $(obj)
	$(AR) rcs $@ $^

.PRECIOUS: $(obj)

clean:
	rm -f $(obj) *.e *.s *.elf *.so* *.bak *.lst *.map *.bin *.bit *.hex *.srec *~
