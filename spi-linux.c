#include "spi.h"

#ifdef __linux__

#include <unistd.h>
#include <fcntl.h>

#include <sys/ioctl.h>
#include <linux/spi/spidev.h>

#include <stdio.h>

#include "debug.h"

#define _SPI_ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

/*
void spi_init(spi_dev_t *dev){
  dev->name = NULL;
  dev->mode = SPI_MODE_DEFAULT;
  dev->bits = 8;
  dev->rate = SPI_RATE_2MHZ;
}
*/

spi_ret_t spi_open(spi_dev_t *dev, spi_hwid_t hwid, spi_mode_t mode, spi_bits_t bits, spi_rate_t rate){
  char name[32];
  snprintf(name, sizeof(name), "/dev/spidev%u.%u", (hwid >> 4) & 0xf, hwid & 0xf);
  
  //LOG("SPI open %s", name);
  if((dev->_fd = open(name, O_RDWR)) < 0) return SPI_CANT_OPEN_DEVICE;
  
  dev->mode = mode;
  uint8_t mode_ = (dev->mode & SPI_CLK_PHASE ? SPI_CPHA : 0) | (dev->mode & SPI_CLK_POLARITY ? SPI_CPOL : 0)
    | (dev->mode & SPI_CSN_NO ? SPI_NO_CS : 0) | (dev->mode & SPI_CSN_HI ? SPI_CS_HIGH : 0)
    | (dev->mode & SPI_LSB_FRST ? SPI_LSB_FIRST : 0);
  if(ioctl(dev->_fd, SPI_IOC_RD_MODE, &mode_) < 0) return SPI_CANT_SET_MODE;
  if(ioctl(dev->_fd, SPI_IOC_WR_MODE, &mode_) < 0) return SPI_CANT_SET_MODE;
  
  dev->bits = bits;
  if(ioctl(dev->_fd, SPI_IOC_RD_BITS_PER_WORD, &dev->bits)) return SPI_CANT_SET_BITS;
  if(ioctl(dev->_fd, SPI_IOC_WR_BITS_PER_WORD, &dev->bits)) return SPI_CANT_SET_BITS;
  
  dev->rate = rate;
  if(ioctl(dev->_fd, SPI_IOC_RD_MAX_SPEED_HZ, &dev->rate)) return SPI_CANT_SET_RATE;
  if(ioctl(dev->_fd, SPI_IOC_WR_MAX_SPEED_HZ, &dev->rate)) return SPI_CANT_SET_RATE;
  
  return SPI_NO_ERROR;
}

spi_ret_t spi_close(spi_dev_t *dev){
  //if(dev->_fd < 0) return SPI_NOT_OPENED;
  
  close(dev->_fd);
  dev->_fd = -1;
  
  return SPI_NO_ERROR;
}

spi_ret_t spi_transfer(spi_dev_t *dev, const spi_val_t *tx, spi_val_t *rx, spi_len_t len){
  if(dev->_fd < 0) return SPI_NOT_OPENED;
  
  struct spi_ioc_transfer mesg[] = {{
	  .tx_buf = (unsigned long)tx,
		.rx_buf = (unsigned long)rx,
		.len = len,
		.delay_usecs = 0,
		.speed_hz = dev->rate,
		.bits_per_word = dev->bits,
		//.cs_change = dev->mode & SPI_CSN_NO ? 0 : 1
	}};
	
  if(ioctl(dev->_fd, SPI_IOC_MESSAGE(_SPI_ARRAY_SIZE(mesg)), mesg) < 0){
    //LOG("SPI transfer error");
    return SPI_CANT_TRANSFER_MSG;
  }
  
  //LOG("SPI transfer ok");
  return SPI_NO_ERROR;
}

#ifdef SPI_ERROR_STRING
const char* _spi_ret_[] = {
  "no error",
  "can't open device",
  "can't set mode",
  "can't set bits per word",
  "can't set max speed hz",
  "device already closed",
  "can't transfer message"
};

const char* spi_error(spi_ret_t ret){
  return _spi_ret_[ret];
}
#endif//SPI_ERROR_STRING

#endif//__linux__
