ifneq ($(rf24.pgm),no)
  def+=RF24_USE_PGMSPACE=1
endif

rf24.retry_delay?=1500
ifneq ($(rf24.retry_delay),)
  def+=RF24_DEFAULT_RETRY_DELAY=$(if $(filter MIN MAX,$(rf24.retry_delay)),RF24_RETRY_DELAY_)$(rf24.retry_delay)
endif

rf24.retry_count?=MAX
ifneq ($(rf24.retry_count),)
  def+=RF24_DEFAULT_RETRY_COUNT=$(if $(filter MIN MAX,$(rf24.retry_count)),RF24_RETRY_COUNT_)$(rf24.retry_count)
endif

rf24.channel?=76
ifneq ($(rf24.channel),)
  def+=RF24_DEFAULT_CHANNEL=$(rf24.channel)
endif

rf24.power_amp?=MAX
ifneq ($(rf24.power_amp),)
  def+=RF24_DEFAULT_POWER_AMP=$(if $(filter MIN LOW HIGH MAX,$(rf24.power_amp)),RF24_POWER_AMP_)$(rf24.power_amp)
endif

rf24.data_rate?=1MBS
ifneq ($(rf24.data_rate),)
  ifneq ($(filter 1MBS 2MBS 250KBS,$(rf24.data_rate)),)
    def+=RF24_DEFAULT_DATA_RATE=RF24_DATA_RATE_$(rf24.data_rate)
  else
    $(error rf24.data_rate must be one of 1MBS 2MBS 250KBS)
  endif
endif

rf24.crc?=16
ifneq ($(rf24.crc),)
  ifneq ($(filter NO 8 16,$(rf24.crc)),)
    def+=RF24_DEFAULT_CRC=RF24_CRC_$(rf24.crc)
  else
    $(error rf24.crc must be one of NO 8 16)
  endif
endif

rf24.payload_size?=MAX
ifneq ($(rf24.payload_size),)
  def+=RF24_DEFAULT_PAYLOAD_SIZE=$(if $(filter MIN MAX,$(rf24.payload_size)),RF24_PAYLOAD_SIZE_)$(rf24.payload_size)
endif

rf24.dynamic_payloads?=0
ifneq ($(rf24.dynamic_payloads),)
  def+=RF24_DEFAULT_DYNAMIC_PAYLOADS=$(rf24.dynamic_payloads)
endif

rf24.poll_interval?=50
ifneq ($(rf24.poll_interval),)
  def+=RF24_WRITE_POLL_INTERVAL=$(rf24.poll_interval)
  def+=RF24_READ_POLL_INTERVAL=$(rf24.poll_interval)
endif
