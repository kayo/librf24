#include "gpio.h"

#ifdef __AVR__

#include <avr/io.h>
#include <stddef.h>

#ifdef GPIO_USE_PGMSPACE
#  include <avr/pgmspace.h>
#else//GPIO_USE_PGMSPACE
#  define PROGMEM
#  define pgm_read_byte(addr) (*(addr))
#  define pgm_read_word(addr) (*(addr))
#endif//GPIO_USE_PGMSPACE

typedef volatile uint8_t* gpio_iop_t;

#define _GPIO_CAT(a, b) a ## b

#define _GPIO_ELM(idx) _gpio_ports_[idx]
#define _GPIO_IDX(idx) (pgm_read_byte(&_GPIO_ELM(idx).gpio))
#define _GPIO_REG(reg, off...) (*((gpio_iop_t)pgm_read_word(&(dev->_port->reg)) off))
#define _GPIO_MSK(msk) (pgm_read_byte(&(dev->_port->msk)))

#ifndef GPIO_USE_OFFSETS
#  define _GPIO_PIN _GPIO_REG(pin)
#  define _GPIO_DDR _GPIO_REG(ddr)
#  define _GPIO_PORT _GPIO_REG(port)
#else//GPIO_USE_OFFSETS
static const int8_t _gpio_pin_ = &PINA-&PORTA;
static const int8_t _gpio_ddr_ = &DDRA-&PORTA;
#  define _GPIO_PIN _GPIO_REG(port, +_gpio_pin_)
#  define _GPIO_DDR _GPIO_REG(port, +_gpio_ddr_)
#  define _GPIO_PORT _GPIO_REG(port)
#endif//GPIO_USE_OFFSETS

#ifndef GPIO_USE_INVERT
#  define _GPIO_HI |= _GPIO_MSK(mask)
#  define _GPIO_LO &= _GPIO_MSK(ksam)
#else//GPIO_USE_INVERT
#  define _GPIO_HI |= _GPIO_MSK(mask)
#  define _GPIO_LO &= ~_GPIO_MSK(mask)
#endif//GPIO_USE_INVERT

#define _GPIO_IS & _GPIO_MSK(mask)

struct _gpio_port_{
  gpio_pin_t gpio;
#ifndef GPIO_USE_OFFSETS
  gpio_iop_t ddr;
  gpio_iop_t pin;
#endif//GPIO_USE_OFFSETS
  gpio_iop_t port;
  gpio_pin_t mask;
#ifndef GPIO_USE_INVERT
  gpio_pin_t ksam;
#endif//GPIO_USE_INVERT
};

const gpio_port_t _gpio_ports_[] PROGMEM = {
#ifndef GPIO_USE_OFFSETS
#  define _GPIO_PORT_REFS(port) &_GPIO_CAT(DDR, port), &_GPIO_CAT(PIN, port), &_GPIO_CAT(PORT, port)
#else//GPIO_USE_OFFSETS
#  define _GPIO_PORT_REFS(port) &_GPIO_CAT(PORT, port)
#endif//GPIO_USE_OFFSETS
#ifndef GPIO_USE_INVERT
#  define _GPIO_PIN_MASKS(pin) _BV(pin), ~_BV(pin)
#else////GPIO_USE_INVERT
#  define _GPIO_PIN_MASKS(pin) _BV(pin)
#endif//GPIO_USE_INVERT
#define _(gpio, port, pin) { gpio, _GPIO_PORT_REFS(port), _GPIO_PIN_MASKS(pin) },
GPIO_PORTS
#undef _
};

#define _GPIO_PORTS (sizeof(_gpio_ports_)/sizeof(gpio_port_t))

/*
void gpio_init(gpio_dev_t* dev){
  dev->_port = NULL;
}
*/

gpio_ret_t gpio_open(gpio_dev_t* dev, gpio_pin_t pin){
  dev->_pin = pin;
  
  for(uint8_t i = 0; i < _GPIO_PORTS; i++){
    if(_GPIO_IDX(i) == pin){
      dev->_port = &_GPIO_ELM(i);
      return GPIO_NO_ERROR;
    }
  }
  
  return GPIO_CANT_SETUP_PIN;
}

gpio_ret_t gpio_close(gpio_dev_t* dev){
  if(dev->_port == NULL){
    return GPIO_NOT_OPENED;
  }
  
  dev->_port = NULL;
  
  return GPIO_NO_ERROR;
}

void gpio_set_dir(gpio_dev_t* dev, gpio_val_t dir){
  if(dir){
    _GPIO_DDR _GPIO_HI;
  }else{
    _GPIO_DDR _GPIO_LO;
  }
}

gpio_val_t gpio_get_dir(gpio_dev_t* dev){
  return _GPIO_DDR _GPIO_IS ? 1 : 0;
}

void gpio_set_val(gpio_dev_t* dev, gpio_val_t val){
  if(val){
    _GPIO_PORT _GPIO_HI;
  }else{
    _GPIO_PORT _GPIO_LO;
  }
}

gpio_val_t gpio_get_val(gpio_dev_t* dev){
  return _GPIO_PIN _GPIO_IS ? 1 : 0;
}

#endif
