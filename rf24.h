#ifndef __RF24_H__
#define __RF24_H__

#include "spi.h"
#include "gpio.h"

typedef /*enum _rf24_power_amp*/ uint8_t rf24_power_amp_t;
typedef /*enum _rf24_crc*/ uint8_t rf24_crc_t;
typedef /*enum _rf24_hardware*/ uint8_t rf24_hardware_t;
typedef /*enum _rf24_data_rate*/ uint8_t rf24_data_rate_t;
typedef /*enum _rf24_write_poll*/ uint8_t rf24_write_poll_t;
typedef uint8_t rf24_channel_t;
typedef uint8_t rf24_retry_t;

typedef struct _rf24_dev rf24_dev_t;

enum _rf24_power_amp{
  RF24_POWER_AMP_MIN = 0,
  RF24_POWER_AMP_LOW,
  RF24_POWER_AMP_HIGH,
  RF24_POWER_AMP_MAX
};

enum _rf24_crc{
  RF24_CRC_NO = 0,
  RF24_CRC_8,
  RF24_CRC_16
};

enum _rf24_hardware{
  RF24_HARDWARE_RF24L01,
  RF24_HARDWARE_RF24L01P
};

enum _rf24_data_rate{
  RF24_DATA_RATE_1MBS = 0,
  RF24_DATA_RATE_2MBS,
  RF24_DATA_RATE_250KBS
};

enum _rf24_write_poll{
  RF24_WRITE_COMPLETE = 0,
  RF24_WRITE_OK = 0,
  RF24_WRITE_ACTIVE = 1,
  RF24_WRITE = 1,
  RF24_WRITE_MAX_RETRY = 2,
  RF24_WRITE_FAIL = 2
};

#define RF24_RETRY_DELAY_MIN 250 //usec
#define RF24_RETRY_DELAY_MAX 4000 //usec
#define RF24_RETRY_DELAY_STEP 250 //usec

#define RF24_RETRY_COUNT_MIN 0
#define RF24_RETRY_COUNT_MAX 15

#define RF24_CHANNEL_MIN 0
#define RF24_CHANNEL_MAX 126
#define RF24_CHANNEL_COUNT (1+RF24_CHANNEL_MAX-RF24_CHANNEL_MIN)

#define RF24_PAYLOAD_SIZE_MIN 1
#define RF24_PAYLOAD_SIZE_MAX 32

typedef uint8_t rf24_val_t;
typedef uint8_t rf24_len_t;
typedef uint16_t rf24_time_t;
typedef void rf24_raw_t;

typedef struct{
  rf24_val_t loss:4;
  rf24_val_t retr:4;
} rf24_write_stat_t;

typedef struct{
  rf24_val_t read_empty:1;
  rf24_val_t read_full:1;
  rf24_val_t write_empty:1;
  rf24_val_t write_full:1;
  rf24_val_t write_reuse:1;
} rf24_queue_stat_t;

//typedef uint64_t rf24_addr_t;
typedef union{
  //uint64_t _;
  rf24_val_t _;
  rf24_val_t _1[1];
  rf24_val_t _5[5];
  rf24_val_t _4[4];
  rf24_val_t _3[3];
} rf24_addr_t;

struct _rf24_dev{
  spi_dev_t spi;
  gpio_dev_t ce;
  
  rf24_hardware_t hardware;
  
  rf24_len_t payload_size;
  rf24_val_t dynamic_payloads;
  
  rf24_len_t ack_payload_size;
  
  rf24_addr_t pipe0_address;
};

void rf24_open(rf24_dev_t *dev, spi_hwid_t spi, gpio_pin_t ce);
void rf24_close(rf24_dev_t *dev);

void rf24_set_listen(rf24_dev_t *dev, rf24_val_t state);

void rf24_set_power(rf24_dev_t *dev, rf24_val_t state);

//rf24_val_t rf24_write_and_wait(rf24_dev_t *dev, const rf24_raw_t *buf, rf24_len_t len, rf24_write_stat_t *stat);
#define rf24_write_with_wait(dev, buf, len, stat) ({ rf24_write((dev), buf, len); rf24_write_wait(dev, stat); })
rf24_write_poll_t rf24_write_poll(rf24_dev_t *dev, rf24_write_stat_t *stat);
rf24_write_poll_t rf24_write_wait(rf24_dev_t *dev, rf24_write_stat_t *stat);
void rf24_write(rf24_dev_t *dev, const rf24_raw_t *buf, rf24_len_t len);

//rf24_len_t rf24_get_payload_size(rf24_dev_t *dev);
#define rf24_get_payload_size(dev) ((dev)->payload_size)
//void rf24_set_payload_size(rf24_dev_t *dev, rf24_len_t size);
#define rf24_set_payload_size(dev, val) {(dev)->payload_size = val;}

void rf24_set_dynamic_payloads(rf24_dev_t *dev, rf24_val_t state);
rf24_len_t rf24_get_dynamic_payload_size(rf24_dev_t *dev);

//rf24_val_t rf24_read_with_wait(rf24_dev_t *dev, rf24_raw_t *buf, rf24_len_t *len, rf24_val_t *pipe, rf24_val_t time);
#define rf24_read_with_wait(dev, buf, len, pipe, time) ({ rf24_read_wait(dev, pipe, time); rf24_read((dev), buf, len); })
rf24_val_t rf24_read_poll(rf24_dev_t *dev, rf24_val_t *pipe);
#if defined(RF24_READ_POLL_INTERVAL)
#  define rf24_read_wait(dev, pipe, time) rf24_read_wait_(dev, pipe, (time)/(RF24_READ_POLL_INTERVAL))
#else
#  define rf24_read_wait(dev, pipe, time) rf24_read_wait_(dev, pipe, time)
#endif
rf24_val_t rf24_read_wait_(rf24_dev_t *dev, rf24_val_t *pipe, rf24_val_t time);
rf24_val_t rf24_read(rf24_dev_t *dev, rf24_raw_t *buf, rf24_len_t *len);

rf24_queue_stat_t rf24_get_queue_stat(rf24_dev_t *dev);

void rf24_open_writing_pipe(rf24_dev_t *dev, rf24_addr_t *addr);
void rf24_open_reading_pipe(rf24_dev_t *dev, rf24_val_t child, rf24_addr_t *addr);

void rf24_set_ack_payload(rf24_dev_t *dev, rf24_val_t state);
void rf24_write_ack_payload(rf24_dev_t *dev, rf24_val_t pipe, const rf24_raw_t *buf, rf24_len_t len);
//rf24_len_t rf24_payload_size(rf24_dev_t *dev);
#define rf24_ack_payload_size(dev) (dev->ack_payload_size)

void rf24_set_auto_ack(rf24_dev_t *dev, rf24_val_t state);
void rf24_set_auto_ack_pipe(rf24_dev_t *dev, rf24_val_t pipe, rf24_val_t state);

rf24_val_t rf24_test_carrier(rf24_dev_t* dev);
rf24_val_t rf24_test_rpd(rf24_dev_t* dev);

void rf24_set_channel(rf24_dev_t *dev, rf24_channel_t chan);
rf24_channel_t rf24_get_channel(rf24_dev_t *dev);

void rf24_set_power_amp(rf24_dev_t* dev, rf24_power_amp_t amp);
rf24_power_amp_t rf24_get_power_amp(rf24_dev_t* dev);

rf24_val_t rf24_set_data_rate(rf24_dev_t* dev, rf24_data_rate_t rate);
rf24_data_rate_t rf24_get_data_rate(rf24_dev_t* dev);

void rf24_set_crc(rf24_dev_t* dev, rf24_crc_t crc);
rf24_crc_t rf24_get_crc(rf24_dev_t* dev);

#define rf24_set_retry(dev, delay, count)                               \
  rf24_set_retry_(dev,                                                  \
                  (((rf24_retry_t)((delay-RF24_RETRY_DELAY_MIN)/RF24_RETRY_DELAY_STEP) & 0xf) << 4) | (count & 0xf))
void rf24_set_retry_(rf24_dev_t* dev, rf24_retry_t retry);

#endif//__RF24_H__
