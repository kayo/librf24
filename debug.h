#ifndef __DEBUG_H__
#define __DEBUG_H__

#ifndef LOG

#  ifdef DEBUG
#    include <stdio.h>
#    define LOG(fmt, args...) printf(fmt "\n", ##args)
#  else
#    define LOG(fmt, args...)
#  endif

#endif

#endif//__DEBUG_H__
