#include "gpio.h"

#ifdef __linux__

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>

void gpio_init(gpio_dev_t* dev){
  dev->_pin = 0;
  
  dev->_dir_fd = -1;
  dev->_val_fd = -1;
}

static inline void write_int_as_str(int fd, gpio_pin_t nv) {
  char str[8];
  snprintf(str, sizeof(str), "%u\n", nv);
  write(fd, str, strlen(str));
}

gpio_ret_t gpio_open(gpio_dev_t* dev, gpio_pin_t pin){
  dev->_pin = 0;
  
  dev->_dir_fd = -1;
  dev->_val_fd = -1;
  
  int fd;
  
  if((fd = open("/sys/class/gpio/export", O_WRONLY)) < 0){
    return GPIO_CANT_SETUP_PIN;
  }
  dev->_pin = pin;
  write_int_as_str(fd, dev->_pin);
  close(fd);
  
  char fn[128];
  
  snprintf(fn, sizeof(fn), "/sys/class/gpio/gpio%u/direction", dev->_pin);
  if((dev->_dir_fd = open(fn, O_WRONLY)) < 0){
    return GPIO_CANT_OPEN_DIR;
  }
  
  snprintf(fn, sizeof(fn), "/sys/class/gpio/gpio%u/value", dev->_pin);
  if((dev->_val_fd = open(fn, O_RDWR)) < 0){
    return GPIO_CANT_OPEN_VAL;
  }
  
  return GPIO_NO_ERROR;
}

gpio_ret_t gpio_close(gpio_dev_t* dev){
  if(dev->_val_fd){
    return GPIO_NOT_OPENED;
  }
  
  int fd;
  
  if((fd = open("/sys/class/gpio/unexport", O_WRONLY)) < 0){
    return GPIO_CANT_SETUP_PIN;
  }
  write_int_as_str(fd, dev->_pin);
  close(fd);
  
  if(dev->_dir_fd > -1){
    close(dev->_dir_fd);
    //dev->_dir_fd = -1;
  }
  
  if(dev->_val_fd > -1){
    close(dev->_val_fd);
    dev->_val_fd = -1;
  }
  
  return GPIO_NO_ERROR;
}

static const char* _gpio_dir_[] = {
  "in\n",
  "out\n"
};

void gpio_set_dir(gpio_dev_t* dev, gpio_val_t dir){
  if(dev->_dir_fd < 0){
    return;
  }
  
  write(dev->_dir_fd, _gpio_dir_[dir & 0x1], dir & 0x1 ? 4 : 3);
}

gpio_val_t gpio_get_dir(gpio_dev_t* dev){
  if(dev->_dir_fd < 0){
    return -1;
  }
  
  char buf[8];
  read(dev->_dir_fd, buf, sizeof(buf));
  return buf[0] == 'o' ? 1 : 0;
}

static const char* _gpio_val_[] = {
  "0\n",
  "1\n"
};

void gpio_set_val(gpio_dev_t* dev, gpio_val_t val){
  if(dev->_val_fd < 0){
    return;
  }
  
  write(dev->_val_fd, _gpio_val_[val & 0x1], 2);
}

gpio_val_t gpio_get_val(gpio_dev_t* dev){
  if(dev->_val_fd < 0){
    return -1;
  }
  
  char buf[8];
  read(dev->_val_fd, buf, sizeof(buf));
  return buf[0] == '1' ? 1 : 0;
}

#ifdef GPIO_ERROR_STRING
const char* _gpio_ret_[] = {
  "ok",
  "off",
  "on",
  "can't open pin setup device",
  "can't open pin direction device",
  "can't open pin value device",
  "device not opened"
};

const char* gpio_error(gpio_ret_t ret){
  return _gpio_ret_[ret];
}
#endif

#endif
