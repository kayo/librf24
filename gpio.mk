gpio.ports?=1:B:3

ifneq ($(gpio.ports),)
  def+=GPIO_PORTS='$(call parsespec,$(gpio.ports))'
endif

ifneq ($(gpio.pgm),no)
  def+=GPIO_USE_PGMSPACE=1
endif

ifeq ($(gpio.opt),s)
  def+=GPIO_USE_OFFSETS=1 GPIO_USE_INVERT=1
endif
